//RAMON TRILLA URTEAGA

import java.io.*;

import acm.util.RandomGenerator;
import acm.program.ConsoleProgram;

/**
 * Problema 10: MergeSort en un fitxer d'accés directe.
 */
public class Problema10 extends ConsoleProgram {

	/**
	 * Nom del fitxer a ordenar.
	 */
	private static final String INPUT = "sorting.dat";

	/**
	 * Nom del primer fitxer auxiliar.
	 */
	private static final String AUX1 = "aux1.dat";

	/**
	 * Nom del segon fitxer auxiliar.
	 */
	private static final String AUX2 = "aux2.dat";

	/**
	 * Quantitat de registres que tindrà el fitxer.
	 */
	private static final int NUM_RECORDS = 100;

	/**
	 * Generador de nombres pseudoaleatoris.
	 */
	private static final RandomGenerator RGEN = RandomGenerator.getInstance();

	/**
	 * Programa principal.
	 */
	public void run() {
		try {
			RandomAccessFile raf = new RandomAccessFile (INPUT, "rw");
			fillFile (raf);
			raf.close();
			boolean sorted = split (INPUT, AUX1, AUX2);
			while (!sorted) {
				merge (AUX1, AUX2, INPUT);
				sorted = split (INPUT, AUX1, AUX2);
			}
			println ("File sorted!");
		} catch (IOException ex) {
			println ("Some error has happened");
		}
	}

	/**
	 * Emplena un fitxer amb persones aleatòries.
	 * @param raf El fitxer
	 */
	private void fillFile (RandomAccessFile raf) throws IOException {
		for (int i = 1; i <= NUM_RECORDS; i++) {
			Person p = createPerson();
			writePerson (raf, p);
		}
	}

	/**
	 * Genera les dades d'una persona.
	 * @return Una persona amb identificador aleatori
	 */
	private Person createPerson () {
		long id = RGEN.nextInt (NUM_RECORDS, 10*NUM_RECORDS-1);
		String name = "Name-" + id;
		int age = RGEN.nextInt (18, 80);
		boolean married = RGEN.nextBoolean();
		return new Person (id, name, age, married);
	}

	/**
	 * Escriu les dades d'una persona a un fitxer.
	 * @param raf El fitxer
	 * @param person La persona
	 */
	private void writePerson (RandomAccessFile raf, Person person) throws IOException {
		byte[] record = person.toBytes();
		raf.write (record);
	}

	/**
	 * Pas split del MergeSort.
	 * @param  input  Nom del fitxer a ordenar
	 * @param output1 Nom del primer fitxer auxiliar
	 * @param output2 Nom del segon fitxer auxiliar
	 * @return Un booleà que indica si ja està ordenat o no
	 */
	private boolean split (String input, String output1, String output2) throws IOException{
		RandomAccessFile  llegir;
		llegir = new RandomAccessFile(input, "r");
		RandomAccessFile aux1;
		aux1 = new RandomAccessFile(output1,"rw");
		aux1.setLength(0);
		RandomAccessFile aux2;
		aux2 = new RandomAccessFile(output2,"rw");
		aux2.setLength(0);
		boolean canvio = true;
		boolean acabado = true;

		Person anterior = readPerson(llegir);
		Person current = readPerson(llegir);
		writePerson(aux1,anterior);
		while (current != null){
			if ((anterior.getId()> current.getId())) {
				acabado = false;
				canvio = !canvio;

			}

			if (canvio){
				writePerson(aux1, current);
			}
			else {
				writePerson(aux2, current);
			}

			anterior = current;
			current = readPerson(llegir);
		}

		return acabado;
	}





	/**
	 * Llegeix les dades d'una persona d'un fitxer.
	 * @param raf El fitxer
	 * @return La persona (o null)
	 */
	private Person readPerson (RandomAccessFile raf) throws IOException {
		byte [] persona = new byte[Person.SIZE] ;

		if (raf.read(persona) !=-1){
			return Person.fromBytes(persona);
		}else {
			return null;
		}
	}

	/**
	 * Pas merge del MergeSort.
	 * @param input1 Nom del primer fitxer auxiliar
	 * @param input2 Nom del segon fitxer auxiliar
	 * @param output Nom del fitxer a ordenar
	 */
	private void merge (String input1, String input2, String output) throws IOException{

		RandomAccessFile aux1;
		aux1 = new RandomAccessFile(input1,"r");
		RandomAccessFile aux2;
		aux2 = new RandomAccessFile(input2,"r");

		RandomAccessFile sortida;
		sortida = new RandomAccessFile(output,"rw");
		sortida.setLength(0);


		Person persones_1 = readPerson(aux1);
		Person persones_2 = readPerson(aux2);


		while ((persones_1 != null) && (persones_2 != null)){
			if (persones_1.getId()< persones_2.getId()){
				writePerson(sortida,persones_1);
				persones_1 = readPerson(aux1);
			}
			else{
				writePerson(sortida,persones_2);
				persones_2 = readPerson(aux2);
			}
		}

		while (persones_1 != null){
			writePerson(sortida,persones_1);
			persones_1 = readPerson(aux1);
		}
		while (persones_2 != null){
			writePerson(sortida,persones_2);
			persones_2 = readPerson(aux2);
		}

	}

}
